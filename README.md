# Medication
Made with expo. 

To run: 

- `yarn add`
- `expo start`

Simulator: 
- Launch AVD/iOS simulator
- In expo developer tools tab click on `'Run on Android device/emulator'` / `'Run on iOS simulator'`
 
Real device: 
- For Android: launch Expo Go app and scan the QR code
- For iOS: open the camera app and scan the QR code, then open it in the Expo Go app
