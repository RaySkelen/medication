import {observable} from "mobx"

const store = observable({
  user: {
    id: 1,
    name: 'Andrew',
    image: 'https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png'
  }
})

export default store