import {createStackNavigator} from '@react-navigation/stack'
import {observer} from 'mobx-react-lite'
import React from 'react'
import Calendar from '../screens/Calendar'
import Dashboard from '../screens/Dashboard'
import Details from '../screens/Details'
import Medication from '../screens/Medication'

const {Navigator, Screen} = createStackNavigator()

export default observer(() => {
  return (
    <>
      <Navigator initialRouteName="dashboard">
        <Screen
          name="dashboard"
          component={Dashboard}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="medication"
          component={Medication}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="calendar"
          component={Calendar}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="details"
          component={Details}
          options={{
            headerShown: false,
          }}
        />
      </Navigator>
    </>
  )
})
