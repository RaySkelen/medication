
import {observer} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import Calendar from '../components/Calendar'
import Footer from '../components/Footer'
import Header from '../components/Header'

export default observer(() => {
  return (
    <View style={styles.container}>
      <View>
        <Header currentScreen="Calendar" />
        <Calendar />
      </View>
      <Footer currentScreen="calendar" />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  }
})