
import {observer} from 'mobx-react-lite'
import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import ContentHeader from '../components/ContentHeader'
import Footer from '../components/Footer'
import Header from '../components/Header'
import MedicationItem from '../components/MedicationItem'
import medicationStore from '../store/Medication.store'

export default observer(() => {
  return (
    <View style={styles.container}>
      <View>
        <Header currentScreen="Medication" />
        <View style={styles.contentContainer}>
          <ContentHeader title="your medications" />
          <ScrollView style={{height: '70%'}} showsVerticalScrollIndicator={false}>
            {medicationStore.items.map(({id, date, text, checked, img}) => {
              return (
                <MedicationItem key={id} date={date} text={text} checked={checked} id={id} img={img}/>
              )
            })}
          </ScrollView>
        </View>
      </View>
      <Footer currentScreen="medication" />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  },
  contentContainer: {
    marginTop: 50,
    paddingRight: 24,
    paddingLeft: 24,
  },
})