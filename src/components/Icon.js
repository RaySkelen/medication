import {Ionicons, MaterialIcons} from '@expo/vector-icons'
import React from 'react'

export default ({type, ...props}) => {
  if (type === 'ionic') {
    return (<Ionicons {...props} />)
  }
  return (<MaterialIcons {...props} />)
}
