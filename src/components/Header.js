
import {useNavigation} from '@react-navigation/core'
import {observer, useLocalObservable} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import HeaderText from './HeaderText'
import Icon from './Icon'

export default observer(({currentScreen, isDetails = false}) => {
  const navigation = useNavigation()
  const store = useLocalObservable(() => ({
    currentScreen
  }))
  return (
    <View style={styles.container}>
      <Icon
        type="ionic"
        name={isDetails ? "chevron-back-outline" : "menu"}
        size={30} color="#9FA2B2"
        style={styles.icon}
        onPress={() => isDetails ? navigation.goBack() : null}
      />
      <HeaderText text={store.currentScreen} style={styles.text} />
      <View style={styles.hiddenElement}><Text>dummy</Text></View>
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    paddingRight: 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  hiddenElement: {
    opacity: 0,
    height: 0,
    marginLeft: 'auto'
  },
  icon: {
    position: 'relative',
    left: 24,
    marginRight: 'auto'
  },
  text: {
    left: '50%',
  }
})