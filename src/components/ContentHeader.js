
import {observer} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import HeaderText from './HeaderText'

export default observer(({title}) => {
  return (
    <View style={styles.container}>
      <HeaderText text={title} />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    fontSize: 24,
    paddingBottom: 10
  }
})
